import dominio.*;

import java.time.LocalDate;

public class Main {
    public static void main(String[] args) {

        Curso curso1 = new Curso();
        Mentoria mentoria1 = new Mentoria();

        curso1.setTitulo("Cusso java");
        curso1.setDescricao("descrição do curso de java");
        curso1.setCargaHoraria(40);

        mentoria1.setTitulo("Mentoria");
        mentoria1.setDescricao("Mentoria do curso java");
        mentoria1.setData(LocalDate.now());

        System.out.println(curso1);
        System.out.println(mentoria1);

        BootCamp bootCamp = new BootCamp();
        bootCamp.setNome("Bootcamp Java Develop");
        bootCamp.setDescricao("Descriçãod o bootcamp develop java");
        bootCamp.getConteudos().add(curso1);
        bootCamp.getConteudos().add(mentoria1);

        Dev devEdivan = new Dev();
        devEdivan.setNome("Edivan");
        devEdivan.increverBootCamp(bootCamp);
        System.out.println("Conteudos incritos: " + devEdivan.getConteudosInscritos());
        devEdivan.progredir();
        System.out.println("Conteudos concluidos: " + devEdivan.getConteudosConcluidos());

        Dev devAluno = new Dev();
        devAluno.increverBootCamp(bootCamp);
        devAluno.setNome("aluno");
        System.out.println("Conteudos incritos: " + devAluno.getConteudosInscritos());
        System.out.println("Conteudos concluidos: " + devAluno.getConteudosConcluidos());
        devAluno.progredir();


    }
}